
var counter = 0;
function changeStar(id){
    var star = $('#'+id).html();
    if(star.includes("gray")) {
        counter++;
        $('#'+id).html("<i class='fa fa-star' style = 'color : orange'></i>");
        $("#jumlahBuku").html("<i class='fa fa-star'style = 'color : orange'></i> " +counter + " ");
    }
    else{
        counter--;
        $('#'+id).html("<i class='fa fa-star' style = 'color : gray'></i>");
        $("#jumlahBuku").html("<i class='fa fa-star'style = 'color : orange'></i> " +counter + " ");
    }   
}

$(document).ready(function(){
    $.ajax({
        url: "/fileJSON/",
        success: function(obj){
            obj = obj.items;
            $("#tableBuku").append("<thead> <th> Title </th> <th> Author </th> <th> Cover </th> <th> Description </th><th> Favorite </th> </thead>");
            $("#tableBuku").append("<tbody>");
            for(c = 0; c < obj.length; c++){
                var title = obj[c].volumeInfo.title;
                var author = obj[c].volumeInfo.authors;
                var image = obj[c].volumeInfo.imageLinks.thumbnail;
                var description = obj[c].volumeInfo.description;
                $("#tableBuku").append("<tr><td>" + title +  "</td> <td> "+ author + "</td> <td>" + "<img src='"+ image + "'>" +"</td> <td>" + description + "</td> <td>" + 
                                    "<button class='button' style = 'background-color: Transparent; border: none' id='"+ obj[c].id + 
                                    "'onclick = 'changeStar(" +"\""+ obj[c].id+"\""+")'><i class='fa fa-star'style = 'color : gray'></i></button>"+"</td></tr>");
            }
            $("#tableBuku").append("</tbody>");
        }
    });
});
