
var counter = 0;
function changeStar(id){
    var star = $('#'+id).html();
    if(star.includes("gray")) {
        counter++;
        $('#'+id).html("<i class='fa fa-star' style = 'color : orange'></i>");
        $("#jumlahBuku").html("<i class='fa fa-star'style = 'color : orange'></i> " +counter + " ");
    }
    else{
        counter--;
        $('#'+id).html("<i class='fa fa-star' style = 'color : gray'></i>");
        $("#jumlahBuku").html("<i class='fa fa-star'style = 'color : orange'></i> " +counter + " ");
    }   
}


$(document).ready(function(){
    $.ajax({
        url: "/fileJSON/",
        success: function(obj){
            obj = obj.items;
            $("#tableBuku").append("<thead> <th> Title </th> <th> Author </th> <th> Cover </th> <th> Description </th><th> Favorite </th> </thead>");
            $("#tableBuku").append("<tbody>");
            for(c = 0; c < obj.length; c++){
                var title = obj[c].volumeInfo.title;
                var author = obj[c].volumeInfo.authors;
                var image = obj[c].volumeInfo.imageLinks.thumbnail;
                var description = obj[c].volumeInfo.description;
                $("#tableBuku").append("<tr><td>" + title +  "</td> <td> "+ author + "</td> <td>" + "<img src='"+ image +
                                         "'>" +"</td> <td>" + description + "</td> <td>" + 
                                        "<button class='button' style = 'background-color: Transparent; border: none' id='"+ 
                                        obj[c].id + 
                                        "'onclick = 'changeStar(" +"\""+ obj[c].id+"\""+
                                        ")'><i class='fa fa-star'style = 'color : gray'></i></button>"+"</td></tr>");
            }
            $("#tableBuku").append("</tbody>");
        }
    });
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$(document).ready(function() {
    $("h1").bind("mouseover", function(){
        var x = $(this).css("background-color");
        var y = $(this).css("color");

        $(this).css("background", "black");
        $(this).css("color", "white");

        $(this).bind("mouseout", function(){
            $(this).css("background", x);
            $(this).css("color", y);
        })    
    })    
})

$(document).ready(function(){
    $("#flip").click(function(){
        $("#panel").slideToggle("slow");
    });
});

$(document).ready(function(){
    $("#flip2").click(function(){
        $("#panel2").slideToggle("slow");
    });
});

$(document).ready(function(){
    $("#flip3").click(function(){
        $("#panel3").slideToggle("slow");
    });
});

$(document).ready(function() {
    $("#ubahbutton").click(function(){
        var b = $("body").css("background-color");
        var c = $(".jumbotron").css("background-color");
        var d = $("h1").css("background-color");

        $("body").css("background", "#e1f2ea");
        $(".jumbotron").css("background", "#fedfaa", "!important");
        $("h1").css("background", "#fedfaa");

        $("#ubahbutton").click(function(){
            $("body").css("background-color", b);
            $(".jumbotron").css("background-color", c);
            $("h1").css("background-color",d );
        })    
    })    
})


$ (function(){
    $(".preloader").fadeOut(2000,function(){
        $(".mycontent").fadeIn(1000);
    });
});

var counter = 0;
function changeStar(id){
    var star = $('#'+id).html();
    if(star.includes("gray")) {
        counter++;
        $('#'+id).html("<i class='fa fa-star' style = 'color : yellow'></i>");
        $("#jumlahBuku").html("<i class='fa fa-star'style = 'color : yellow'></i> " +counter + " ");
    }
    else{
        counter--;
        $('#'+id).html("<i class='fa fa-star' style = 'color : gray'></i>");
        $("#jumlahBuku").html("<i class='fa fa-star'style = 'color : yellow'></i> " +counter + " ");
    }   
}

$(document).ready(function(){
    $.ajax({
        url: "/data_json",
        success: function(obj){
            obj = obj.items;
            $("#tableBuku").append("<thead> <th> Title </th> <th> Author </th> <th> Cover </th> <th> Description </th><th> Favorite </th> </thead>");
            $("#tableBuku").append("<tbody>");
            for(c = 0; c < obj.length; c++){
                var title = obj[c].volumeInfo.title;
                var author = obj[c].volumeInfo.authors;
                var image = obj[c].volumeInfo.imageLinks.thumbnail;
                var description = obj[c].volumeInfo.description;
                $("#tableBuku").append("<tr><td>" + title +  "</td> <td> "+ author + "</td> <td>" + "<img src='"+ image + "'>" +"</td> <td>" + description + "</td> <td>" + 
                                    "<button class='button' style = 'background-color: Transparent; border: none' id='"+ obj[c].id + 
                                    "'onclick = 'changeStar(" +"\""+ obj[c].id+"\""+")'><i class='fa fa-star'style = 'color : gray'></i></button>"+"</td></tr>");
            }
            $("#tableBuku").append("</tbody>");
        }
    });
});

// $(document).ready(function(){
//     var timer;
//     $("#id_email").keyup(function(e){
//         clearTimeout(timer)
//         timer = setTimeout(function(a){
//             cekValid();
//         },150);
//     });
// });

// $(document).ready(function(){
//     var timer;
//     $("#id_nama").keyup(function(e){
//         clearTimeout(timer)
//         timer = setTimeout(function(a){
//             cekValid();
//         },150);
//     });
// });

// $(document).ready(function(){
//     var timer;
//     $("#id_password").keyup(function(e){
//         clearTimeout(timer)
//         timer = setTimeout(function(a){
//             cekValid();
//         },150);
//     });
// });

// function cekValid(){
//     $.ajax({
//         url: "/check_valid",
//         data : {
//             email : $("#id_email").val(),
//             nama : $("#id_nama").val(),
//             password : $("#id_password").val()
//         },
//         success: function(obj){
//             $("#hasil").html(obj.message)
//             if(obj.message == "Email nama dan password valid")
//                 document.getElementById("submit").disabled = false
//             else{
//                 document.getElementById("submit").disabled = true
//             }
//         }
//     });
// };

var is_unique = false;
$(function() {
    $("#id_email").change(function() {
        console.log( $(this).val() );
        email = $(this).val();
		console.log("hehehehe");
        $.ajax({
            method: "POST",
            url: '{% url "check_valid/" %}',
            data: {
                'email': email
            },
            dataType: 'json',
            success: function (data) {
                console.log(data)
                if (data.is_taken) {
                    alert("Email sudah terdaftar sebelumnya, silakan pakai email lain");
                    is_unique = false;
                }
                else{
                    is_unique = true;
                }
            }
        });
    });
    $('#id_email').on('keyup', function () {
        check_submit();
    });
    $('#id_nama').on('keyup', function () {
        check_submit();
    });
    $('#id_password').on('keyup', function () {
        check_submit();
    });
});

function create_post() {
    $.ajax({
        url : "add_subscriber/", // the endpoint
        type : "POST", // http method
        data : { 
            email : $('#id_email').val(), 
            nama : $('#id_nama').val(), 
            password : $('#id_password').val() }, // data sent with the post request
        success : function(json) {
            $('#id_email').val(''); // remove the value from the input
            $('#id_nama').val('');
            $('#id_password').val('');
        },
    });
};

function check_submit(){
    var panjang_nama = $('#id_nama').val().length;
    var panjang_email = $('#id_email').val().length;
    var panjang_password = $('#id_password').val().length;

    if(panjang_nama > 0 && panjang_email > 0 && panjang_password > 0 && is_unique) {
        $('#submit_regis').prop('disabled', false);
    } else {
        $('#submit_regis').prop('disabled', true);
    }
}

function submit_post() {
    $.ajax({
        url : "/add_subscriber/", // the endpoint
        type : "POST", // http method
        data : { nama : $('#id_nama').val(), email : $('#id_email').val(), password : $('#id_password').val() }, // data sent with the post request

        // handle a successful response
        success : function(json) {
            $('#id_nama').val(''); // remove the value from the input
            $('#id_email').val(''); // remove the value from the input
            $('#id_password').val(''); // remove the value from the input
            $('#submit_regis').prop('disabled', true);
        },
    });
}

function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
  }