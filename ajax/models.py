from django.db import models
from django.core.validators import MinLengthValidator

class registeredSubscriber(models.Model):
	email = models.EmailField(max_length=70,blank=True, null= True, unique= True, error_messages={'unique': 'An account with this email exist.'})
	nama = models.CharField(max_length = 50)
	password = models.CharField(validators=[MinLengthValidator(4)], max_length = 20)

