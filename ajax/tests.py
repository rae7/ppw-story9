from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import home, booksJson
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.

class AjaxTest(TestCase):
    def test_ajax_page_url_exist(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)
    
    def test_ajax_using_home_func(self):
        found = resolve('/')
        self.assertEquals(found.func, home)

    def test_ajax_page_using_ajax_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_JSON_page_url_exist(self):
        response = Client().get('/fileJSON/')
        self.assertEquals(response.status_code, 200)
    
    def test_JSON_using_booksJson_func(self):
        found = resolve('/fileJSON/')
        self.assertEquals(found.func, booksJson)