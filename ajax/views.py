from django.shortcuts import render, redirect
from . import forms
from ajax.forms import Subscribe_Form
from .models import registeredSubscriber
from django.http import HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout
import requests

response = {}
# Create your views here.
def home(request):
    return render(request, 'index.html', response)

@csrf_exempt	
def add_subscriber(request):
	response = {}
	if (request.method == "POST" or None):
		response['email'] = request.POST['email']
		response['nama'] = request.POST['nama']
		response['password'] = request.POST['password']
		subscriber = registeredSubscriber(email = response['email'], nama = response['nama'], password = response['password'])
		subscriber.save()
	return JsonResponse({})

@csrf_exempt	
def check_valid(request):
	email = request.POST.get('email', None)
	data = {
		'is_taken': registeredSubscriber.objects.filter(email=email).exists()
	}
	return JsonResponse(data)

def index_subscribe(request):
	forms = registeredSubscriber.objects.all()
	response = {'form' : Subscribe_Form, 'result': forms}
	return render(request, "subs.html", response)


def booksJson(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=quilting"
    data = requests.get(url).json()
    return JsonResponse(data)

def logoutView(request):
	logout(request)
	return HttpResponseRedirect('/')

def googleVerv(request):
	return render(request, 'googleb2bc53023b6c32e4.html')

def my_view (request):
	username = request.POST['username']
	password = request.POST['password']
	user = authenticate(request, username = username, password = password)
